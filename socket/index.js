let port = process.env.PORT || 8900
const io = require("socket.io")(port, {
    cors: {
      origin: "*",
    },
  });

  
  let users = [];  

  const addUser = (userId, socketId) => {
    !users.some((user) => user.userId.id_user === userId.id_user) &&
    users.push({ userId, socketId });
  };
  
  const removeUser = (socketId) => {
    users = users.filter((user) => user.socketId !== socketId);
  };
  
  const getUser = (userId) => {
    return users.find((user) => user.userId.id_user === userId);
  };
  
  io.on("connection", (socket) => {
    //when ceonnect
    // console.log("a user connected.");
  
    //take userId and socketId from user
    socket.on("addUser", (userId) => {
      addUser(userId, socket.id);
      io.emit("getUsers", users);
    });
  
    //send and get message
    socket.on("sendMessage", ({ sender, receiver, text }) => {
      const user = getUser(receiver);
      const userSender = getUser(sender)
      // console.log(user);
      if(user === undefined){
      }else{
        console.log(user);
        io.to(user.socketId).emit("getMessage",
        {
          profile_image: userSender.userId.profile_image,
          username: userSender.userId.username,
          sender,
          text,
        });
      }
    });


  //   socket.on("sendNotification", ({ sender, receiver, text }) => {
  //     const user = getUser(receiver);
  //     const userSender = getUser(sender)
  //     // console.log(user);
  //     if(user === undefined){
  //     }else{
  //       // console.log(user);
  //       io.to(user.socketId).emit("getNotification",
  //       {
  //         profile_image: userSender.userId.profile_image,
  //         username: userSender.userId.username,
  //         sender,
  //         text,
  //       });
  //     }
  //   });
  
    //when disconnect
    socket.on("disconnect", () => {
      console.log("a user disconnected!");
      removeUser(socket.id);
      io.emit("getUsers", users);
    });

  });

  console.log(`Server on port ${port}`);