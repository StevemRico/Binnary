import { Request, Response } from "express";
import { stringify } from "querystring";
import { getRepository } from "typeorm";
import { Conversation } from "../entity/Conversation";
import { Messages } from "../entity/Messages";

export const PostMessage = async (req: Request, res: Response): Promise<Response> => {
    const {sender, receiver, text} = req.body;
    // Logica de si envia o no el id de la conversacion
    try {
        const getChat = await getRepository(Conversation).findOne({relations: ['sender','receiver'],where: [{sender: sender,receiver: receiver},{sender: receiver,receiver: sender}]})
        if(getChat === undefined){            
            const newChat = getRepository(Conversation).create({sender,receiver});
            const result = await getRepository(Conversation).save(newChat);
            const chatId:any = result.id_conversation;
            const newMessage = getRepository(Messages).create({ text, sender, id_conversation:chatId })
            const postMessage = getRepository(Messages).save(newMessage);
            return res.json(postMessage);
        }else{
            const chatId:any = getChat.id_conversation;
            const newMessage = getRepository(Messages).create({ text, sender, id_conversation:chatId })
            const postMessage = getRepository(Messages).save(newMessage);
            // console.log(newMessage);
            
            return res.json(newMessage);
        }
    } catch (error) {
        return res.status(500).json(error);
    }
}

export const CreateConversation = async (req: Request, res: Response): Promise<Response> => {
    const {sender, receiver} = req.body;    
    try {
        const getChat = await getRepository(Conversation).findOne({relations: ['sender','receiver'],where: [{sender: sender,receiver: receiver},{sender: receiver,receiver: sender}]})
        if(getChat === undefined){            
            const newChat = getRepository(Conversation).create({sender,receiver});
            const result = await getRepository(Conversation).save(newChat);
            const chatId:any = result.id_conversation;
            return res.json(chatId);
        }else{
            return res.json('Conversacion ya creada');
        }
    } catch (error) {
        return res.status(500).json(error);
    }
}

export const GetConversation = async (req: Request, res: Response): Promise<Response> => {
    const {sender} = req.body;
    try {
        const getChat = await getRepository(Conversation).find({relations: ['sender','receiver'],where: [{sender: sender},{receiver: sender}]})
        return res.json(getChat)
    } catch (error) {
        return res.status(500).json(error);
    }
}

export const GetMessage = async (req: Request, res: Response): Promise<Response> => {
    const {id_conversation} = req.body;
    try {
        const getMessage = await getRepository(Messages).find({order:{id_message: "ASC"},relations:['sender'],where:{id_conversation: id_conversation}});
        return res.json(getMessage);
    } catch (error) {
        return res.status(500).json(error);
    }
}