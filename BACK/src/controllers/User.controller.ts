import { Request, Response } from "express";
import { getRepository, Like } from "typeorm";
import { Users } from '../entity/User';
import { Follows } from '../entity/Follows';
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Notifications } from "../entity/Notifications";
const saltRounds = 10;

export const Login = async (req: Request, res: Response): Promise<Response> => {
  try {
    const User = await getRepository(Users).findOne({ username: req.body.username });
    if (User) {
      let PassHash = bcrypt.compareSync(req.body.password, User.password);
      if (PassHash) {
        // const token: string = jwt.sign({_id: Users.id_user}, 'TOKEN_SECRET' || '', {expiresIn: '3s'})
        // return res.header('auth-token', token).json(token);
        const token = jwt.sign({ User }, 'TOKEN_SECRET')
        return res.json(token);
      } else {
        return res.json("Usuario y/o contraseña incorrectas");
      }
    } else {
      // console.log("El usuario no existe");
      return res.json("El usuario no existe");
    }
  } catch (error) {
    return res.status(500).json(error)
  }
}

export const Register = async (req: Request, res: Response): Promise<Response> => {
  try {
    const Username = await getRepository(Users).findOne({ username: req.body.username });
    const Email = await getRepository(Users).findOne({ email: req.body.email });
    const Phone = await getRepository(Users).findOne({ phone_number: req.body.phone_number });

    if (Username) {
      return res.json("Username ya en uso");
    } else if (Email) {
      return res.json("Email ya en uso");
    } else if (Phone) {
      return res.json("Phone ya en uso");
    } else {
      const salt = bcrypt.genSaltSync(saltRounds);
      const hash = bcrypt.hashSync(req.body.password, salt);
      const roleStatus: any = 1;
      const UserBody = {
        username: req.body.username,
        email: req.body.email,
        password: hash,
        phone_number: req.body.phone_number,
        user_status: roleStatus,
        role: roleStatus,
        // profile_image: "https://binnaryback.herokuapp.com/public/Profile-Pictures/Profile-Image.png",
        profile_image: "http://localhost:3030/public/Profile-Pictures/Profile-Image.png",
        description: req.body.description,
        birthday_date: req.body.birthday_date,
        login_date: req.body.login_date,
      }
      const newUser = getRepository(Users).create(UserBody);
      await getRepository(Users).save(newUser)
      return res.json("Usuario Registrado");
    }
  } catch (error) {
    return res.status(500).json(error);
  }
}

export const UserGet = async (req: Request, res: Response): Promise<Response> => {
  try {
    const user = await getRepository(Users).findOne(req.params.id);
    const followers = await getRepository(Follows).find({ relations: ['id_user_follower'], where: [{ id_user_follow: req.params.id }] });
    const follows = await getRepository(Follows).find({ relations: ['id_user_follow'], where: [{ id_user_follower: req.params.id }] });    
    const userData = {
      username: user?.username,
      email: user?.email,
      phone_number: user?.phone_number,
      profile_image: user?.profile_image,
      description: user?.description,
      followers_length: followers.length,
      follow_length: follows.length,
      followers: followers,
      follows: follows
    }    
    return res.status(200).json(userData);
  } catch (err) {
    return res.status(500).json(err);
  }
}

export const CurrentUserGet = async (req: Request, res: Response) => {
  try {
    const token = req.header('token');
    if (!token) {
      return res.json('Access Denied');
    } else {
      jwt.verify(token, 'TOKEN_SECRET', async (err, authdata) => {
        if (authdata) {
          const user = await getRepository(Users).findOne(authdata.User?.id_user);
    const followers = await getRepository(Follows).find({ relations: ['id_user_follower'], where: [{ id_user_follow: req.params.id }] });
    const follows = await getRepository(Follows).find({ relations: ['id_user_follow'], where: [{ id_user_follower: req.params.id }] });    
    const userData = {
      id_user: user?.id_user,
      username: user?.username,
      email: user?.email,
      phone_number: user?.phone_number,
      profile_image: user?.profile_image,
      description: user?.description,
      followers_length: followers.length,
      follow_length: follows.length,
      followers: followers,
      follows: follows
    }    
    return res.status(200).json(userData);
          return res.json(userData);
        } else {
          return res.json({ user: 'No existe session alguna' });
        }
      });
    }
  } catch (err) {
    return res.status(500).json(err);
  }
}

export const UserUpdate = async (req: Request, res: Response) => {
  const User = await getRepository(Users).findOne({where: {id_user: req.body.id_user}})
  if(User){
    try {
      const Username = await getRepository(Users).findOne({ username: req.body.username });
      const Email = await getRepository(Users).findOne({ email: req.body.email });
      const Phone = await getRepository(Users).findOne({ phone_number: req.body.phone_number });
    if (User?.username != req.body.username && Username) {
        return res.json("Username ya en uso");
    } else if (User?.email != req.body.email && Email) {
      return res.json("Email ya en uso");
    } else if (User?.phone_number != req.body.phone_number && Phone) {
      return res.json("Phone ya en uso");
    } else {
      const file = 'http://localhost:3030/public/Profile-Pictures' + '/' + req.file?.filename;
      // const file = 'https://binnaryback.herokuapp.com/public/Profile-Pictures' + '/' + req.file?.filename;
      let UserBody = {}
      if(req.file?.fieldname === undefined){
        UserBody = {
          username: req.body.username,
          email: req.body.email,
          phone_number: req.body.phone_number,
          profile_image: User.profile_image,
          description: req.body.description        
        }
      }else{
        UserBody = {
          username: req.body.username,
          email: req.body.email,
          phone_number: req.body.phone_number,
          profile_image: file,
          description: req.body.description        
        }
      }
      await getRepository(Users).update(req.body.id_user,UserBody)
      return res.json(UserBody);
    }
  } catch (error) {
    return res.status(500).json(error);
  }
  }else{
    return res.json("Y el usuario papu?");
  }
}

export const UserUpdatePassword = async (req: Request, res: Response): Promise<Response> => {
  return res.json("update User")
}

// export const UserDelete = async (req: Request, res: Response): Promise<Response> => {
// }

export const UserFollow = async (req: Request, res: Response): Promise<Response> => {
  const userFollowed = await getRepository(Follows).findOne({ where: { id_user_follower: req.body.id_user, id_user_follow: req.params.id } });
  if (userFollowed) {
    const paramsid: any = req.params.id
    const UserFollowedData = { id_user_follower: req.body.id_user, id_user_follow: paramsid }
    const Return = await getRepository(Follows).delete(UserFollowedData);
    return res.json("unlike effective");
  } else {
    // console.log({
    //   "userparams": req.params.id,
    //   "user": req.body.id_user
    // });
    
    if (req.body.id_user !== req.params.id) {
      const paramsid: any = req.params.id
      const FollowData = { id_user_follower: req.body.id_user, id_user_follow: paramsid }
      const Return = await getRepository(Follows).save(FollowData);
      return res.json(Return);
    } else {
      return res.status(403).json("You cant Follow yourself");
    }
  }
}


export const UserFind = async (req: Request, res: Response): Promise<Response> => {
  try {
    const user = await getRepository(Users).find({
      username: Like(`%${req.params.find}%`)
    })
    if (user) {
      return res.json(user);
    } else {
      return res.status(403).json("Without Results")
    }
  } catch (error) {
    return res.status(500).json(error);
  }
}

export const GetNotification = async (req: Request, res: Response): Promise<Response> => {
  try {
    const Notification = await getRepository(Notifications).find({order: {id_notification: "DESC"},relations: ['sender','receiver'],where: [{receiver: req.params.id}]});
    return res.json(Notification);
  } catch (error) {
    return res.status(500).json(error);
  }
}

export const PostNotification = async (req: Request, res: Response): Promise<Response> => {
  const {sender, receiver, text} = req.body;
    // Logica de si envia o no el id de la conversacion
    if(sender === receiver){
      return res.json('No puedes enviarte Notificacion a ti mismo')
    }else{
      try {
        await getRepository(Notifications).findOne({relations: ['sender','receiver'],where: [{sender: sender,receiver: receiver},{sender: receiver,receiver: sender}]})
         const newNotification = getRepository(Notifications).create({ text, sender, receiver })
         getRepository(Notifications).save(newNotification);
         return res.json(newNotification);
   } catch (error) {
       return res.status(500).json(error);
   }
    }
}