import {Entity,Column,PrimaryGeneratedColumn,JoinColumn,ManyToOne} from 'typeorm';
import { Users } from './User';

@Entity()
export class Notifications{
    @PrimaryGeneratedColumn()
    id_notification: number;
    @Column()
    text: string
    @Column({
        type: "timestamp", default: () => "CURRENT_TIMESTAMP"
    })
    sended_at: Date;
    @ManyToOne(() => Users, user => user.id_user)
    @JoinColumn()
        sender : Users
    @ManyToOne(() => Users, user => user.id_user)
    @JoinColumn()
        receiver : Users
}