import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Conversation } from './Conversation';
import { Users } from './User';

@Entity()
export class Messages {
    @PrimaryGeneratedColumn()
    id_message: number;
    @Column()
    text: string
    @Column({
        type: "timestamp", default: () => "CURRENT_TIMESTAMP"
    })
    sended_at: Date;
    @ManyToOne(() => Users, user => user.id_user)
    @JoinColumn()
        sender : Users
    @ManyToOne(() => Conversation, conver => conver.id_conversation)
    @JoinColumn()
        id_conversation: Conversation
}