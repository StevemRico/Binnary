import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from 'typeorm';
// import { Messages } from './Messages';
import { Users } from './User';

@Entity()
export class Conversation {
    @PrimaryGeneratedColumn()
    id_conversation: number;
    @ManyToOne(() => Users, user => user.id_user)
    @JoinColumn()
    sender : Users
    @ManyToOne(() => Users, user => user.id_user)
    @JoinColumn()
    receiver : Users
    @Column({
        type: "timestamp", default: () => "CURRENT_TIMESTAMP"
    })
    sended_at: Date;
}    
    