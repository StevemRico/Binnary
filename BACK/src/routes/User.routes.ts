import { Router } from "express";
import {TokenValidation} from '../libs/verifyToken';
const router = Router();

import {CurrentUserGet, GetNotification, Login, PostNotification, Register, UserFind, UserFollow, UserGet, UserUpdate} from "../controllers/User.controller";
import uploadPP from "../libs/UpdloadProfileImage";

router.post("/Register", Register);
router.post("/Login", Login);
router.get("/UserGet/:id",TokenValidation, UserGet);
router.put("/UserUpdate",TokenValidation, uploadPP.single('image') , UserUpdate);
router.get("/UserFind/:find",TokenValidation, UserFind);
router.get("/CurrentUserGet",TokenValidation, CurrentUserGet);
router.post('/:id/Follow',TokenValidation, UserFollow);
router.get('/Notification/:id',TokenValidation, GetNotification);
router.post('/Notification',TokenValidation, PostNotification);


export default router;
