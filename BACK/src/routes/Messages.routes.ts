import {Router} from "express";
import { CreateConversation, GetConversation, GetMessage, PostMessage } from "../controllers/Messages.controller";
const router = Router();

import upload from "../libs/uploadImage";
import { TokenValidation } from "../libs/verifyToken";

router.post('/PostMessage', TokenValidation, PostMessage);
router.post('/GetConversation', TokenValidation, GetConversation);
router.post('/CreateConversation',TokenValidation, CreateConversation);
router.post('/GetMessage',TokenValidation, GetMessage);

export default router;