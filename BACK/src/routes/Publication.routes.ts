import {Router} from "express";
const router = Router();

import {CommentPublication, DeleteCommentPublication, DeletePublication, getPublications, getPublicationsUser, getPublicationsVideos, getPublicationUnique, LikePublication, postPublication,UnLikePublication, UpdatePublication} from "../controllers/publication.controller";
import uploadI from "../libs/uploadImage";
import uploadV from "../libs/uploadVideo";
import { TokenValidation } from "../libs/verifyToken";

router.get('/Publication',TokenValidation,getPublications )
router.get('/PublicationUser/:id',TokenValidation,getPublicationsUser )
router.post('/Publication' , TokenValidation, uploadI.single('image') , postPublication)
router.get('/Publication/:id', TokenValidation,getPublicationUnique)
router.put('/Publication/Update/:id', TokenValidation,UpdatePublication)
router.delete('/Publication/Delete', TokenValidation,DeletePublication)
router.post('/Publication/Like', TokenValidation,LikePublication)
router.delete('/Publication/UnLike', TokenValidation,UnLikePublication)
router.post('/Publication/Comment', TokenValidation,CommentPublication)
router.delete('/Publication/Delete/Comment', TokenValidation,DeleteCommentPublication)
router.get('/PublicationVideos', TokenValidation, getPublicationsVideos)

export default router;