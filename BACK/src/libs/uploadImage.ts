import multer from 'multer'
import path from 'path'
import {v4 as uuid} from 'uuid'
import {v2 as cloudinary} from 'cloudinary';

cloudinary.config({ 
    cloud_name: 'binnary', 
    api_key: '426953964277257', 
    api_secret: 'QxOUXynFPYLJcIQcPiB9hw7dexQ' 
  });

// Settings
const storage = multer.diskStorage({
    
    
    destination: path.join(__dirname, '../../public/Publications-Pictures'),
    // destination: path.join(__dirname, 'https://drive.google.com/drive/folders/1DzOVuuiW4GULGPhnrFP6iF53abe8yVkS?usp=sharing'),
    filename: (req,file,cb) => {
        // cb(null, Date.now() + '.' + file.mimetype.split('/')[1])
        cb(null, Date.now() + uuid() + path.extname(file.originalname))
    }
});
export default multer({storage});