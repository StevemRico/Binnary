import React, { useEffect, useReducer, useState } from 'react'
import UserReducer from "./UserReducer";
import UserContext from "./UserContext";
import axios from 'axios';
import { url } from '../../assets/env';
import { useLocalStorage } from '../../hooks/useLocalStorage';

const UserState = (props) => {

    const [Token, setToken] = useLocalStorage('token','');
    const [UserC, setUserC] = useLocalStorage('user','');
    const initialState = {
        CurrentUser: {
            id_user: "",
            username: "",
            email: "",
            phone_number: "",
            profile_image: "",
            description: ""
        },
        chat: {
            chatId : '',
            receiverId : ''
        },
        search: null
    }
    const [state, dispatch] = useReducer(UserReducer, initialState);

    const GetCurrentUser = async () => {
        function headerToken(Token) {
            axios.interceptors.request.use(
                config => {
                    config.headers = { 'token': Token };
                    return config;
                }
            );
        }
        headerToken(Token);
        await axios.get(`${url}CurrentUserGet`)
            .then(response => {
                dispatch({
                    type: 'GET_CURRENT_USER',
                    payload: response.data
                });
                setUserC(response.data);
            })
    }

    const GetChat = async (chat) => {
        dispatch({
            type: 'GET_CURRENT_CHAT',
            payload: chat
        })
    }

    const GetSearch = async (search) => {
        dispatch({
            type: 'GET_SEARCH',
            payload: search
        })
    }

    return (
        <UserContext.Provider value={{ GetCurrentUser, GetChat, GetSearch, CurrentUser: state.CurrentUser, chat: state.chat, search: state.search }}>
            {props.children}
        </UserContext.Provider>
    )

}

export default UserState;