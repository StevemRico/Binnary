import { GET_CURRENT_USER,GET_CURRENT_CHAT,GET_SEARCH } from "../types";

// eslint-disable-next-line import/no-anonymous-default-export
export default (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_CURRENT_USER:
      return {
        ...state,
        CurrentUser: payload,
      };
    case GET_CURRENT_CHAT:
      return {
        ...state,
        chat: payload,
      };
    case GET_SEARCH:
      return {
        ...state,
        search: payload,
      };
    default:
      return state;
  }
};