import axios from 'axios';
import { url } from '../assets/env';

export function headerToken(Token) {
    axios.interceptors.request.use(
        config => {
            config.headers = { 'token': Token };
            return config;
        }
    );
}

export async function GetPublications(Token) {
    headerToken(Token);
    return await axios.get(`${url}Publication`)
        .then(response => {
            // console.log(response.data);
            const Publications = response.data;
            return Publications;
        })
}

export async function GetVideos(Token) {
    headerToken(Token);
    return await axios.get(`${url}PublicationVideos`)
        .then(response => {
            // console.log(response.data);
            const Publications = response.data;
            return Publications;
        })
}

export async function GetPublicationsUser(Token,id_user) {
    headerToken(Token);
    return await axios.get(`${url}PublicationUser/${id_user}`)
        .then(response => {
            // console.log(response.data);
            const Publications = response.data;
            return Publications;
        })
}

export async function GetPublicationUnique(Token,id_publication) {
    headerToken(Token);
    return await axios.get(`${url}Publication/${id_publication}`)
        .then(response => {
            // console.log(response.data);
            const Publication = response.data;
            return Publication;
        })
}