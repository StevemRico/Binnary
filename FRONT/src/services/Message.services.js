import axios from 'axios';
import { url } from '../assets/env';

export function headerToken(Token) {
    axios.interceptors.request.use(
        config => {
            config.headers = { 'token': Token };
            return config;
        }
    );
}

export async function GetConversation(Token,id) {
    headerToken(Token);
    const conversation = {
        sender: id
    }
    return await axios.post(`${url}getConversation`,conversation)
        .then(response => {
            // console.log(response.data);
            const conver = response.data;
            return conver;
        })
}

export async function GetMessage(Token,id) {
    headerToken(Token);
    const conversation = {
        id_conversation: id
    }
    return await axios.post(`${url}getMessage`,conversation)
        .then(response => {
            // console.log(response.data);
            const messa = response.data;
            return messa;
        })
}

export async function PostMessage(Token,Message) {
    headerToken(Token);
    return await axios.post(`${url}PostMessage`,Message)
        .then(response => {
            // console.log(response.data);
            const messa = response.data;
            return messa;
        })
}

export async function CreateConversation(Token,Members) {
    headerToken(Token);
    return await axios.post(`${url}CreateConversation`,Members)
        .then(response => {
            // console.log(response.data);
            const Conversation = response.data;
            return Conversation;
        })
}