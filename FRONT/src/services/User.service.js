import axios from 'axios';
import { url } from '../assets/env';
import { io } from "socket.io-client";

export function headerToken(Token) {
    axios.interceptors.request.use(
        config => {
            config.headers = { 'token': Token };
            return config;
        }
    );
}

export async function GetFindUser(Token, userFind) {
    headerToken(Token);
    return await axios.get(`${url}UserFind/${userFind}`)
        .then(response => {
            // console.log(response.data);
            const UserFind = response.data;
            return UserFind;
        })
}
export async function GetUserProfile(Token, userId) {
    headerToken(Token);
    return await axios.get(`${url}UserGet/${userId}`)
        .then(response => {
            // console.log(response.data);
            const UserFind = response.data;
            return UserFind;
        })
}
export async function UpdateUserProfile(Token, Update) {
    headerToken(Token);
    return await axios.put(`${url}UserUpdate`, Update)
        .then(response => {
            // console.log(response.data);
            const UserFind = response.data;
            return UserFind;
        })
}

export async function FollowUnFollowService(Token, User) {
    // console.log(User);
    const options = {
        method: 'POST',
        url: `${url}${User.follow}/Follow`,
        headers: {
            'Content-Type': 'application/json',
            token: Token
        },
        data: { id_user: User.follower }
    };

    return await axios.request(options)
        .then(response => {
            // console.log(response.data);
            return response.data;
        }).catch(function (error) {
            console.error(error);
        });
}

export async function GetNotification(Token, User) {
    headerToken(Token);
    return await axios.get(`${url}Notification/${User}`)
        .then(response => {
            // console.log(response.data);
            const Noti = response.data;
            return Noti;
        })
}



export async function PostNotification(Token, User) {
    const socket = io("ws://localhost:8900");
    // const socket = io("https://binnarysocket.herokuapp.com/");
    headerToken(Token);
    // console.log(User);
    socket.emit("sendNotification", {
        sender: User.sender,
        receiver: User.receiver,
        text: User.text,
      });
    return await axios.post(`${url}Notification`, User)
        .then(response => {
            // console.log(response.data);
            const Noti = response.data;
            return Noti;
        })
}