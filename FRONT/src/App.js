import './App.css';
import {BrowserRouter,Navigate,Route,Routes} from 'react-router-dom';
import {useLocalStorage} from './hooks/useLocalStorage';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import Sidebar from './components/Sidebar/Sidebar';
import Home from './views/Home/Home'
import Messenger from './views/Messenger/Messenger';
import Profile from './views/Profile/Profile';
import FindUser from './views/FindUser/FindUser';
import Videos from './views/Videos/Videos';
import PostPublication from './views/PostPublication/PostPublication';
import PublicationUnique from './components/PublicationUnique/PublicationUnique';
import ProfileSettings from './views/ProfileSettings/ProfileSettings';
function App() {
  const [Token,setToken] = useLocalStorage('token','');
  if(Token === ''){
    return (
        <BrowserRouter>
        <Routes>
          <Route path='/' element={<Navigate to='/Login' />} />
          <Route path='/Login' element={<Login />} />
          <Route path='/Register' element={<Register />} />
        </Routes>
        </BrowserRouter>
    )
  }else{
    return (
        <BrowserRouter>
          <Sidebar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/Home' element={<Home />} />
            <Route path='/Messages' element={<Messenger />} />
            <Route path='/Profile/:id' element={<Profile />} />
            <Route path='/ProfileSettings/:id' element={<ProfileSettings />} />
            <Route path='/FindUser' element={<FindUser />} />
            <Route path='/Videos' element={<Videos />} />
            <Route path='/PostPublications' element={<PostPublication />} />
            <Route path='/Publication/:id' element={<PublicationUnique />} />
          </Routes>
        </BrowserRouter>
    )
  }
}

export default App;
