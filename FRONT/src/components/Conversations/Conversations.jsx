import './Conversations.css';
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { useContext, useEffect, useState } from 'react';
import { GetConversation } from '../../services/Message.services';
import UserContext from '../../context/User/UserContext';

export default function Conversations() {
    const [Token,setToken] = useLocalStorage('token','');
    const [Conversation, setConversation] = useState([]);
    const {GetChat} = useContext(UserContext);
    const [UserC, setUserC] = useLocalStorage('user','');
    let ConverU = null;
    useEffect(()=>{
        GetConversation(Token,UserC.id_user).then(conver => setConversation(conver));
    },[])
    const SendChat = (id,receiver) => {
        GetChat({chatId: id, receiverId: receiver});
    }
    for (let i = 0; i < Conversation.length; i++) {
        if(UserC.id_user === Conversation[i].sender.id_user){
            ConverU = true;
        }else{
            ConverU = false;
        }
        
    }

    return (
        <>
            {
                Conversation.map(chat => {
                    return(
                        <div className="Conversations" key={chat.id_conversation}  onClick={() => SendChat(chat.id_conversation, ConverU ? chat.receiver.id_user : chat.sender.id_user )}>
                            {
                                ConverU ? <img className='Conversation-Img' src={chat.receiver.profile_image } alt="" /> : <img className='Conversation-Img' src={chat.sender.profile_image} alt="" />
                            }
                            <span className='Conversation-Name' >
                                {
                                    ConverU ? <span>{chat.receiver.username}</span> : <span>{chat.sender.username}</span>
                                }
                            </span>
                        </div>
                    )
                })
            }
        </>
    )
}
