import React, { useContext, useEffect, useRef, useState } from 'react'
import UserContext from '../../context/User/UserContext';
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetMessage } from '../../services/Message.services';
import { PostMessage } from '../../services/Message.services';
import './Messages.css';

import { io } from "socket.io-client";
import { PostNotification } from '../../services/User.service';

export default function Messages() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [Message, setMessage] = useState([]);
    const [UserCu, setUserCu] = useLocalStorage('user', '');
    const [receiver, setreceiver] = useLocalStorage('receiver', '');
    const { chat } = useContext(UserContext);
    const [Real,setReal] = useState({sender: '',text: ''})
    const [newMessage, setnewMessage] = useState({});
    const scrollReft = useRef();
    let ChatExist = null;
    const socket = io("ws://localhost:8900");
    // const socket = io("https://binnarysocket.herokuapp.com");
    if (chat === undefined) { ChatExist = false; } else { ChatExist = true; }
    const UserC = (id) => { if (id === UserCu.id_user) { return true } else { return false } }
    let NewConver = null;
    useEffect(() => { 
        GetMessage(Token, chat.chatId).then(messa => setMessage(messa)); 
        socket.emit("addUser", UserCu);
    }, [chat.chatId])
    useEffect(() => {
         scrollReft.current?.scrollIntoView({ behavior: "smooth" }) 
    }, [Message])
    // console.log(receiver);
    if(chat.chatId){
        NewConver = false;
    }else{
        NewConver = true;
    }
    // NewConver ? console.log('receiver') : console.log('chat')
    // console.log(NewConver ? receiver : chat.receiverId)
    useEffect(() => {
        setnewMessage({
            "id_message": Math.random(),
            "receiver": NewConver ? receiver : chat.receiverId,
            "sender": {
                "id_user": UserCu.id_user,
                "profile_image": UserCu.profile_image,
                "username": UserCu.username,
            }
        });
        // console.log(newMessage);
    }, [chat])

    useEffect(() => {
        socket.on("getMessage", (data) => {
          setReal(data);
        });
    }, []);
    
    useEffect(()=>{
        setMessage([
            ...Message,
            {
                "id_message": Math.random(),
                "receiver": chat.receiverId,
                "sender": {
                    "id_user": Real.sender,
                    "profile_image": Real.profile_image,
                    "username": Real.username,
                },
                "text": Real.text
            }
        ])
        // console.log("Hola");
    },[Real])
    // console.log(newMessage);
    const PostChat = () => {
        if(newMessage.text){
            PostMessage(Token, newMessage).then(newMess => {
                setMessage([...Message, {
                    "id_message": Math.random(),
                    "sender": UserCu.id_user,
                    "receiver": chat.receiverId,
                    "sender": {
                        "id_user": UserCu.id_user,
                        "profile_image": UserCu.profile_image,
                        "username": UserCu.username,
                    },
                    "text": newMess.text
                }]);
            })
            socket.emit("sendMessage", {
                sender: UserCu.id_user,
                receiver: chat.receiverId,
                text: newMessage.text,
              });
              
        PostNotification(Token,{sender:UserCu.id_user, receiver: chat.receiverId, text: "Te envio un mensaje"});
            }else{
         }
    }
    const handleSubmit = e => { e.preventDefault(); }
    const handleChange = async e => {
        setnewMessage({
            ...newMessage,
            [e.target.name]: e.target.value
        })
    }

    let MessageMap = Message.map(messa => {
        return (
            <div className={UserC(messa.sender.id_user) ? "Messages own" : "Messages"} key={messa.id_message} ref={scrollReft}>
                <div className="MessageTop">
                    <img className='Message-Img' src={messa.sender.profile_image} alt="" />
                    <div className="">
                        <span className='Message-Name'>{messa.sender.username}</span>
                        <p className='Message-Text'>{messa.text}</p>
                    </div>
                </div>
                <div className="MessageBottom">
                    {
                        // messa.sended_at
                    }
                </div>
            </div>
        )
    })

    return (
        <>
            {
                ChatExist ? MessageMap : ""
            }
            <div className="ChatBoxBottom">
                <div className='MessagePost'>
                    <form action="" onSubmit={handleSubmit}>
                        <div className="MessagePostWrapper">
                            <textarea placeholder='Escribe algo..' className='ChatBox-Message-Input' value={newMessage?.text} name='text' id='text' onChange={handleChange} required></textarea>
                            <button className='ChatBox-Message-Botton' id='SendMessageButton' onClick={() => PostChat()} >Enviar</button>
                        </div>
                    </form>
                </div>

            </div>
        </>
    )
}
