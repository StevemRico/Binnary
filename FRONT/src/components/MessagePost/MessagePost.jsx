import React, { useContext, useEffect, useRef, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { PostMessage } from '../../services/Message.services';
import { PostNotification } from '../../services/User.service';
import './MessagePost.css';
export default function MessagePost(props) {
    const [UserC,setUserC] = useLocalStorage('user','');
    const [Token, setToken] = useLocalStorage('token', '');
    const [newMessage, setnewMessage] = useState({});
    useEffect(()=>{
        setnewMessage({"sender": UserC.id_user,"receiver": props.receiver});
    },[props])
        
    // const ScrollReft = useRef();
    const PostChat = () => {
        PostMessage(Token, newMessage).then(window.location.href='/');
        // console.log("Hola");
    }
    const handleSubmit = e => { e.preventDefault(); }
    const handleChange = async e => {
        setnewMessage({
            ...newMessage,
            [e.target.name]: e.target.value
        })
    }

    return (
        <div className='MessagePost'>
            <form action="" onSubmit={handleSubmit}>
            <div className="MessagePostWrapper">
                <textarea placeholder='Escribe algo..' className='ChatBox-Message-Input' value={newMessage?.text} name='text' id='text' onChange={handleChange}></textarea>
                <button className='ChatBox-Message-Botton' id='SendMessageButton' onClick={() => PostChat()}>Enviar</button>
            </div>
            </form>
        </div>
    )
}
