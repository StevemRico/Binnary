import React, { useContext, useState } from "react";
import { useLocalStorage } from "../../hooks/useLocalStorage";
import { BsSearch } from "react-icons/bs";
import {Link} from 'react-router-dom'
import "./Search.css";
import UserContext from "../../context/User/UserContext";
export default function Search() {
  const [Token, setToken] = useLocalStorage("token", "");
  const [FindUsers, setFindUsers] = useState();
  const {GetSearch} = useContext(UserContext);
  const handleChange = async (e) => {
    setFindUsers({
          ...FindUsers,
          [e.target.name]: e.target.value,
      });
  };
  const Find = () => {
    GetSearch(FindUsers?.search);
  };
  return (
    <div className="search-box">
      <input
        type="text"
        placeholder="Search anything"
        className="search-input"
        name='search'
        id='search'
        onChange={handleChange}
        autoComplete='off'
      />
      {/* <a href="#" className="search-btn">
        <BsSearch />
      </a> */}
      <button className="search-btn" onClick={Find}>
        <Link to='FindUser' className='search-btn-link'>
          <BsSearch />
        </Link>
      </button>
    </div>
  );
}
