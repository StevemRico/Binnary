import React, { useContext, useEffect, useState } from 'react'
import './Comments.css';
import { IoIosSend } from 'react-icons/io'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import axios from "axios";
import { PostNotification } from '../../services/User.service';
import { url } from '../../assets/env';

export default function Comments(props) {
    const [Token, setToken] = ('token', '');
    const [Comment, setComment] = useState('');
    const [Comments, setComments] = useState([]);
    const [UserC, setUserC] = useLocalStorage('user', '');
    useEffect(() => {
        setComments(props.comments);
    }, [])
    const handleChange = async (e) => {
        setComment({
            ...Comment,
            [e.target.name]: e.target.value,
        });
    };
    const SendComment = () => {
        if (Comment?.text === undefined) {
        } else {
            // console.log({ text: Comment.text, id_user: UserC.id_user, id_publication: props.publication, content: 1 });
            const options = {
                method: 'POST',
                url: `${url}Publication/Comment`,
                headers: {
                    'Content-Type': 'application/json',
                    token: Token
                },
                data: { text: Comment.text, id_publication: props.publication, id_user: UserC.id_user }
            };

            axios.request(options).then(function (response) {
                setComments([ ...Comments,
                    {
                        "id_comment": Math.random(),
                        "text": Comment.text,
                        "id_user": {
                            "id_user": UserC.id_user,
                            "profile_image": UserC.profile_image,
                            "username": UserC.username,
                        }
                    }
                ])
                // console.log(props.userData);
                PostNotification(Token,{sender:UserC.id_user, receiver: props.userData.id_user, text: "Comentó tu publicacion"});
                // Tiene que cambiar el ultimo por el que se quiere enviar
                // window.location.reload();
        }).catch (function (error) {
            console.error(error);
        });
    }
}
if(props.comments === undefined){
    return (
        <div className='Comments'>
            <span>
            0 Commentarios...
            </span>
            <br />
            <div className="Comments-Button">
                <input type="text" name="text" id="text" placeholder='Send a Comment' onChange={handleChange} required />
                <button onClick={SendComment} id='button-comment'><IoIosSend /></button>
            </div>
        </div>
    )
}else{
    return (
        <div className='Comments'>
            {
                Comments.slice((props.comments.length - props.quantity), props.comments.length).map(comments => {
                    return (
                        <div className="" key={comments.id_comment}>
                            <span>
                                <img className='Comments-Profile-Img' src={comments.id_user.profile_image} alt="" />
                                <label htmlFor="" className='Comments-Profile-Username'>{comments.id_user.username}</label>
                            </span>
                            <span>
                                {comments.text}
                            </span>
                            <br />
                            <span>
                                {props.comments.length} Comentarios...
                            </span>
                            <br />
                        </div>
                    )
                })
            }
            <div className="Comments-Button">
                <input type="text" name="text" id="text" placeholder='Send a Comment' onChange={handleChange} required />
                <button onClick={SendComment} id='button-comment'><IoIosSend /></button>
            </div>
        </div>
    )
}

}
