import React, { useEffect, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetNotification } from '../../services/User.service';
import './Notifications.css';

import { io } from "socket.io-client";

export default function Notifications() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [UserC, setUserC] = useLocalStorage('user', '');
    const [Notifications, setNotifications] = useState([]);
    const [Real,setReal] = useState({sender: '',text: ''})
    const socket = io("ws://localhost:8900");
    // const socket = io("https://binnarysocket.herokuapp.com/");
    useEffect(() => {
        GetNotification(Token, UserC.id_user).then(noti => setNotifications(noti));
    }, [])

    // console.log(Notifications);
    return (
        <div className="Notifications-Collapsed" id='Notification'>
            <div className="NotificationsWrapper">
                {
                    Notifications.map(noti => {
                        return (
                            <div className="Notification-Message" key={noti.id_notification}>
                                <img src={noti.sender.profile_image} alt="" />
                                <div className="">
                                    <span> <b> {noti.sender.username} </b> </span>
                                    <br />
                                    <span>{noti.text}</span>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
