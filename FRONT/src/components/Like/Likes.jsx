import { useContext, useEffect, useState } from 'react'
import {FcLike,FcLikePlaceholder} from 'react-icons/fc';
import {useLocalStorage} from '../../hooks/useLocalStorage';
import axios from 'axios';
import { url } from '../../assets/env';
import './Like.css';
import UserContext from '../../context/User/UserContext';
import { PostNotification } from '../../services/User.service';

export default function Likes(props) {
    const [Token,setToken] = useLocalStorage('token','');
    const [LikeIcon,setLikeIcon] = useState(props.likeUser);
    const [UserC, setUserC] = useLocalStorage('user', '');
    // console.log(props.userData);
    let LikeCurrentUser = null;
    const LikePost = () => {
        PostNotification(Token,{sender:UserC.id_user, receiver: props.userData.id_user, text: "Dio Like a tu publicacion"});
        axios.post(`${url}Publication/Like`,{id_user: UserC.id_user, id_publication: props.publi})
            .then(res => {
                // console.log(res);
            })
        setLikeIcon(1);
    }
    const UnLikePost = () => {
        axios.post(`${url}Publication/Like`,{id_user: UserC.id_user, id_publication: props.publi})
            .then(res => {
                // console.log(res);
            })        
        setLikeIcon(0);
    }
    if(props.like === undefined){

    }else{
        for (let i = 0; i < props.like.length; i++) {
            if(UserC.id_user === props.like[i].id_user){
                console.log("Hola");
                LikeCurrentUser = true;
            }else{
                LikeCurrentUser = false;
            }
        }
    }
    
    if(LikeIcon === 1 || LikeCurrentUser === true){
        return (
            <div className="Likes">
                <FcLike onClick={UnLikePost}/>
            </div>
        )   
    }else{
        return (
            <div className="Likes">
                <FcLikePlaceholder onClick={LikePost}/>
            </div>
        )
    }
}