import React, { useState, useEffect, useContext } from 'react';
import './Sidebar.css';
import { BsFillGridFill, BsSearch } from 'react-icons/bs';
import { AiOutlinePoweroff, AiFillHome, AiFillMessage } from 'react-icons/ai';
import { BiNotification, BiUpload } from 'react-icons/bi';
import { BsPlayBtn } from 'react-icons/bs';
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { Link } from 'react-router-dom';
import Search from '../Search/Search';
import UserContext from '../../context/User/UserContext';
import Notifications from '../Notifications/Notifications';

export default function Sidebar() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [UserC, setUserC] = useLocalStorage('user', '');
    const {GetCurrentUser} = useContext(UserContext);
  useEffect(()=>{
    GetCurrentUser();
  },[UserC])
    const Logout = () => {
        setToken('');
        window.location.href = '/Login';
    }

    const sidebarexpanded = () => {
        const side = document.querySelector('#sidemenu');
        side.classList.toggle("sidemenu-menu-expanded");
        side.classList.toggle("sidemenu-menu-collapsed");
        // const Noti = document.querySelector('#Notification');
        // Noti.classList.toggle("Notification-Expanded-Side-Expanded");
        // Noti.classList.toggle("Notification-Expanded-Side-Collapsed");
    }
    const NotificationExpand = () => {
        const Noti = document.querySelector('#Notification');
        Noti.classList.toggle("Notifications-Expanded");
        Noti.classList.toggle("Notifications-Collapsed");
    }
    return (
        <>
            <div className="sidemenu-menu-search">
                <Search />
            </div>
            <div className='sidemenu-menu-collapsed' id='sidemenu'>
                <div className='sidemenu-header' onClick={sidebarexpanded}>
                    <BsFillGridFill />
                    <span>Binnary</span>
                </div>

                <Link to={{pathname: `/Profile/${UserC.id_user}`}} className='Link' >
                    <div className='sidemenu-profile' >
                        <div className='sidemanu-img-profile'>
                            <img src={UserC.profile_image} alt='0' />
                        </div>
                        <div className='sidemenu-username'>
                            <span>
                                {UserC.username}
                            </span>
                        </div>
                        <div className='sidemenu-follow'>
                        </div>
                    </div>
                </Link>
                <div className='sidemenu-items'>
                    <Link to='/Home' className='Link sidemenu-item'>
                        <div className='sidemenu-item-logo'>
                            <AiFillHome />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Home</span>
                        </div>
                    </Link>
                    <Link to='/PostPublications' className='Link sidemenu-item postpubli'>
                        <div className='sidemenu-item-logo'>
                            <BiUpload />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Post Publications</span>
                        </div>
                    </Link>
                    <Link to='Messages' className='Link sidemenu-item'>
                        <div className='sidemenu-item-logo'>
                            <AiFillMessage />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Messages</span>
                        </div>
                    </Link>
                    <div className='Link sidemenu-item' onClick={NotificationExpand}>
                        <div className='sidemenu-item-logo'>
                            <BiNotification />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Notifications</span>
                        </div>
                    </div>
                    <Link to='Videos' className='Link sidemenu-item'>
                        <div className='sidemenu-item-logo'>
                            <BsPlayBtn />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Videos</span>
                        </div>
                    </Link>
                </div>
                <div className='sidemenu-logout' onClick={Logout}>
                    <Link to='' className='Link sidemenu-item logout'>
                        <div className='sidemenu-item-logo'>
                            <AiOutlinePoweroff />
                        </div>
                        <div className='sidemenu-item-text'>
                            <span>Logout</span>
                        </div>
                    </Link>
                </div>
                <div className="">
                    <Notifications />
                </div>
            </div>
        </>
    )
}