import React, { useEffect, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetPublications } from '../../services/Publications.service';
import { Link } from 'react-router-dom'
import './Publications.css';
import { FcComments, FcShare } from 'react-icons/fc'
import Likes from '../Like/Likes';
import Comments from '../Comments/Comments';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Masonry from 'react-masonry-css'
export default function Publications() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [Publication, setPublication] = useState([]);
    // const [UserC, setUserC] = useLocalStorage('user','');
    useEffect(() => {
        GetPublications(Token).then(publication => setPublication(publication));
    }, [])
    if (Publication === []) {
        return (
            <div className="Publications">
                <div className="PublicatinsWrapper">
                    No hay nada
                </div>
            </div>
        )
    } else {
        return (
            <div className="Publications">
                <div className="PublicationsWrapper">
                <Masonry
  breakpointCols={1}
  className="my-masonry-grid"
  columnClassName="my-masonry-grid_column">
  {
                        Publication.map((publi => {
                            return (
                                <div className="Publication-card" key={publi.id_publication}>
                                    <div className="Publication-img">
                                        <Link to={{pathname: `/Publication/${publi.id_publication}`}} >
                                        <LazyLoadImage
                                            alt={publi.src}
                                            className="Publication-img-lazy"
                                            key={publi.id_publication}
                                            src={publi.file}
                                            // threshold={1}
                                            // wrapperClassName="gallery-img-wrapper" 
                                            />
                                        </Link>
                                    </div>
                                    <div className="Publication-card-bottom">
                                        <div className="Publication-icons">
                                            <div className="Publication-icons-like">
                                            <Likes like={publi.like} likeUser={publi.likeuser} userData={publi.user} publi={publi.id_publication} />
                                            </div>
                                            <div className="Publication-icons-comment">
                                                <FcComments />
                                            </div>
                                            <div className="Publication-icons-share">
                                                <FcShare />
                                            </div>
                                        </div>
                                        <div className="Publication-likes">
                                            {/* <LikeCount lenght={publi.likes} publi={publi.id_publication} /> */}
                                        </div>
                                        <div className="Publication-description">
                                            <div className="Publication-description-User">
                                                <img src={publi.user.profile_image} alt="" />
                                                <span>{publi.user.username}</span>
                                            </div>
                                            <div className="Publication-description-Text">
                                            <span>
                                                {publi.descripcion}
                                            </span>
                                            </div>
                                        </div>
                                        <div className="Publication-comments">
                                            <Comments comments={publi.comments} quantity={1} userData={publi.user} publication={publi.id_publication}/>
                                        </div>
                                    </div>
                                </div>
                            )
                        }))
                    }
</Masonry>
                </div>
            </div>
        )
    }

}
