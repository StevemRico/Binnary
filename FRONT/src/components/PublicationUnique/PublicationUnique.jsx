import React, { useEffect, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetPublicationUnique } from '../../services/Publications.service';
import { FcComments, FcShare } from 'react-icons/fc'
import Likes from '../Like/Likes';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { useParams } from 'react-router';
import Comments from '../Comments/Comments';
import './PublicationUnique.css';
export default function PublicationUnique() {
    let { id } = useParams();
    const [Token, setToken] = useLocalStorage('token', '');
    const [Publication, setPublication] = useState([]);
    useEffect(() => {
        GetPublicationUnique(Token, id).then(publication => setPublication(publication));
    }, [])
    
    return (
        <div className="PublicationUnique">
            <div className="PublicationUniqueWrapper">
                {
                    <div className="PublicationUnique-card" key={Publication.id_publication}>
                        <div className="PublicationUnique-img">
                            <LazyLoadImage
                                alt={Publication.src}
                                className="PublicationUnique-img-lazy"
                                effect='blur'
                                key={Publication.id_publication}
                                src={Publication.file}
                                threshold={1}
                                wrapperClassName="gallery-img-wrapper" />
                        </div>
                        <div className="PublicationUnique-card-bottom">
                            <div className="PublicationUnique-icons">
                                <div className="PublicationUnique-icons-like">
                                    <Likes like={Publication.like} likeUser={Publication.likeuser} userData={Publication.user} publi={Publication.id_publication} />
                                </div>
                                <div className="PublicationUnique-icons-comment">
                                    <FcComments />
                                </div>
                                <div className="PublicationUnique-icons-share">
                                    <FcShare />
                                </div>
                            </div>
                            <div className="PublicationUnique-description">
                                <div className="PublicationUnique-description-User">
                                    {/* <img src={Publication.user.profile_image} alt="" />
                                    <span>{Publication.user.username}</span> */}
                                </div>
                                <div className="PublicationUnique-description-Text">
                                    <span>
                                        {Publication.descripcion}
                                    </span>
                                </div>
                            </div>
                            <div className="PublicationUnique-comments">
                                <Comments comments={Publication.comments} userData={Publication.user} quantity={10} publication={Publication.id_publication}/>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}
