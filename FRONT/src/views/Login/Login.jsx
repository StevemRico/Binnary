import React, { useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import axios from 'axios'
import './Login.css'
import { urlLogin } from '../../assets/env';
import {Link} from 'react-router-dom'

export default function Login() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [Login, setLogin] = useState({ username: '', password: '' });
    const [Response, setResponse] = useState('');
    const handleSubmit = e => { e.preventDefault(); }
    const handleChange = async e => {
        setLogin({
            ...Login,
            [e.target.name]: e.target.value
        })
    }

    const Post = () => {
        if(Login.username === '' || Login.password === ''){
            setResponse('Los campos deben ser llenados');
        }else{
            axios.post(`${urlLogin}`, Login)
                .then(response => {
                    // console.log(response);
                    if(response.data === 'Usuario y/o contraseña incorrectas' || response.data === 'El usuario no existe'){
                        setResponse('Usuario y/o contraseña incorrectas');
                    }else{
                        setToken(response.data);
                        window.location.href = '/';
                    }
                })
        }
    }
    return (
        <div className="Login">
            <div className="LoginWrapper" onSubmit={handleSubmit}>
                <div className="Login-form-group">
                    <h1>
                        Login
                    </h1>
                </div>
                <hr />
                <br /><br />
                <div className="Login-form-group">
                    <input type="text" className='Username' placeholder="Username" name="username" onChange={handleChange} required/>
                </div>
                <div className="Login-form-group">
                    <input type="password" className='Password' placeholder="Password" name="password" onChange={handleChange} required/>
                </div>
                <div className="Login-form-group">
                    {/* <a href="#" className="forgot-password">Forgot password?</a> */}
                    <Link to='/Register' className='Login-Form-Register'>Registrate</Link>
                    <Link to='/Register' className='Login-Form-Forgot'>Olvidaste la contraseña</Link>
                </div>
                <div className="Login-form-group-button">
                <h6>
                {Response}
                </h6>
                    <button onClick={Post}>Log in</button>
                </div>
            </div>
        </div>
    )
}
