import React,{Suspense,lazy} from 'react';
import { useLocalStorage } from '../../hooks/useLocalStorage';
// import Publication from '../../components/Publications/Publication';
import './Home.css';
import { io } from "socket.io-client";
export default function Home() {
    const [UserC,setUser] = useLocalStorage('user','')
    const Publication = lazy(() =>import('../../components/Publications/Publication'));
    const socket = io("ws://localhost:8900");
    // const socket = io("https://binnarysocket.herokuapp.com/");
    socket.emit("addUser", UserC);
    return (
        <div>
            <Suspense fallback={<h1>Loading ...</h1>}>
                <Publication />
            </Suspense>
        </div>
    )
}
