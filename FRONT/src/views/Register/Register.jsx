import React, { useState } from 'react'
import './Register.css'
import axios from 'axios';
import { urlRegister } from '../../assets/env';
import {Link} from 'react-router-dom';

export default function Register() {
    const [Response, setResponse] = useState('')
    const [Register, setRegister] = useState({
        username: '',
        email: '',
        password: '',
        phone_number: ''
    });
    const handleSubmit = e => { e.preventDefatult(); }
    const handleChange = async e => {
        await setRegister({
            ...Register,
            [e.target.name]: e.target.value
        })
    }

    const Post = () => {
        axios.post(`${urlRegister}`, Register)
        .then(response => {
            if(response.data === "Usuario Registrado"){
                window.location.href='/Login';
            }else{
                setResponse(response.data);
            }
        }).catch()
    }
    return (
        <div className="Register">
            <div className="RegisterWrapper" onSubmit={handleSubmit}>
                <div className="Register-form-group">
                    <h1>
                        Register
                    </h1>
                </div>
                <hr />
                <br /><br />
               <div className="Register-form-group">
                    <input type="text" className='Username' placeholder="Username" name="username" onChange={handleChange} required/>
                 </div>
                 <div className="Register-form-group">
                     <input type="email" className='Email' placeholder="Email" name="email" onChange={handleChange} required/>
                 </div>
                 <div className="Register-form-group">
                     <input type="text" className='Phone' placeholder="Phone" name="phone_number" onChange={handleChange} required/>
                 </div>
                 <div className="Register-form-group">
                     <input type="password" className='Password' placeholder="Password" name="password" onChange={handleChange} required/>
                 </div>
                 <div className="Register-form-group">
                     <input type="text" className='description' placeholder="Description" name="description" onChange={handleChange} required/>
                 </div>
                <div className="Register-form-group">
                    {/* <a href="#" className="forgot-password">Forgot password?</a> */}
                    <Link to='/Register' className='Register-Form-Register'>Registrate</Link>
                    <Link to='/Register' className='Register-Form-Forgot'>Olvidaste la contraseña</Link>
                </div>
                <div className="Register-form-group-button">
                <h6>
                {Response}
                </h6>
                    <button onClick={Post}>Register</button>
                </div>
            </div>
        </div>
    )
}
