import React, { useEffect, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { UpdateUserProfile } from '../../services/User.service';
import './ProfileSettings.css';

export default function ProfileSettings() {
    const [UserC, setUserC] = useLocalStorage('user', '');
    const [Token, setToken] = useLocalStorage('token', '');
    const [Archivos, setArchivos] = useState(null);
    const [Description, setDescription] = useState({
        id_user: UserC.id_user,
        username: UserC.username,
        email: UserC.email,
        description: UserC.description,
        image: UserC.profile_image
    });
    const handleSubmit = e => { e.preventDefatult(); }
    const handleChange = async e => {
        setDescription({
            ...Description,
            [e.target.name]: e.target.value
        })
    }
    const subirArchivos = e => {
        setArchivos(e);
      }
    const UpdateProfile = (e) => {
        const form = new FormData();
        form.append("id_user", UserC.id_user);
        form.append("description", Description.description);
        form.append("phone_number", Description.phone_number);
        form.append("username", Description.username);
        form.append("email", Description.email);
        if(Archivos === null){
            form.append('image', UserC.profile_image);
        }else{
            for (let index = 0; index < Archivos.length; index++) {
              form.append('image', Archivos[index]);
            //   console.log(Archivos[index]);
            }
        }
        UpdateUserProfile(Token,form)
                .then(a => {
                    setUserC({
                        id_user: UserC.id_user,
                        username: a.username,
                        email: a.email,
                        description: a.description,
                        phone_number: a.phone_number,
                        profile_image: a.profile_image
                    })
                    window.location.reload();
                })
      }
    return (
        <div className="ProfileSettings">
            <div className="ProfileSettingsWrapper" onSubmit={handleSubmit}>
                <img className='ProfileSettings-Picture' src={UserC.profile_image} alt="" />
                <br />
                {/* <input className='ProfileSettings-Picture-Input' type="file" name="image" id="" onChange={handleChange} /> */}
                <input className='ProfileSettings-Picture-Input' type="file" id="file-upload" onChange={(e) => subirArchivos(e.target.files)} />
                <br /><br />
                <label htmlFor="Username">Username</label>
                <input className='ProfileSettings-Input' type="text" id="Username" name="username" placeholder={UserC.username} onChange={handleChange} />

                <label htmlFor="Email">Email</label>
                <input className='ProfileSettings-Input' type="text" id="Email" name="email" placeholder={UserC.email} onChange={handleChange} />

                {/* <label htmlFor="Phone">Phone</label>
                <input className='ProfileSettings-Input' type="text" id="Phone" name="phone_number" placeholder={UserC.phone_number} onChange={handleChange} /> */}

                <label htmlFor="password">Last Password</label>
                <input className='ProfileSettings-Input' type="password" id="password" name="password" onChange={handleChange} />

                <label htmlFor="password">New Password</label>
                <input className='ProfileSettings-Input' type="password" id="password" name="password" onChange={handleChange} />

                <label htmlFor="password">Description</label>
                <input type="text" className='ProfileSettings-Input' placeholder={UserC.description} name="description" onChange={handleChange} />

                <input className='ProfileSettings-Button' type="submit" value="Update Profile" onClick={()=> UpdateProfile()}/>
            </div>
        </div>
    )
}
