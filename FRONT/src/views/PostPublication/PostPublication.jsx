import React, { useState, useEffect } from 'react'
import Select from 'react-select'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetPublicationsUser } from '../../services/Publications.service';
import './PostPublication.css';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import axios from "axios";
import Masonry from 'react-masonry-css'
import { url } from '../../assets/env';
export default function PostPublication() {
  const [VideoT, setVideoT] = useState({ value: 0 });
  const [Publication, setPublication] = useState([]);
  const [Token, setToken] = ('token', '');
  const [UserC, setUserC] = useLocalStorage('user', '');
  const [PostPublications, setPostPublications] = useState({});
  const [Archivos, setArchivos] = useState(null);
  // console.log(VideoT.value);
  const subirArchivos = e => {
    setArchivos(e);
  }
  const handleSubmit = e => { e.preventDefatult(); }
  const handleChange = async e => {
    await setPostPublications({
      ...PostPublications,
      [e.target.name]: e.target.value,
    })
  }
  // console.log(PostPublications);
  useEffect(() => {
    GetPublicationsUser(Token, UserC.id_user).then((publi) =>
      setPublication(publi)
    );
  }, []);

  const PostPublicationSubmit = (e) => {
    const form = new FormData();
    form.append("description", PostPublications.description);
    form.append("id_user", UserC.id_user);
    form.append("video", VideoT.value);
    for (let index = 0; index < Archivos.length; index++) {
      form.append('image', Archivos[index]);
    }

    const options = {
      method: 'POST',
      url: `${url}Publication`,
      headers: {
        'Content-Type': 'multipart/form-data; boundary=---011000010111000001101001',
        token: Token,
        Authorization: 'Bearer undefined'
      },
      data: form
    };

    axios.request(options).then(response => {
      // console.log(response.data);
      window.location.reload();
    }).catch(function (error) {
      console.error(error);
    });
  }
  const options = [
    { value: '0', label: 'No' },
    { value: '1', label: 'Si' },
  ]

  return (
    <div className="PostPublication">
      <div className="PostPublicationWrapper">
        <div onSubmit={handleSubmit}>
          <h1>Post Publication</h1>
          <input type="text" placeholder='Description' className='PostPublication-Description' name='description' onChange={handleChange} />
          <div className="">
            <h2>Es video</h2>
            <Select options={options} value={VideoT} onChange={setVideoT} className='PostPublication-Select-Video' />
          </div>
          <div className="">
            <input className='PostPublication-File' type="file" id="file-upload" onChange={(e) => subirArchivos(e.target.files)} />
          </div>
          <div className="">
            <button className='PostPublication-Submit' onClick={() => PostPublicationSubmit()}>Post Publication</button>
          </div>
        </div>
        <div className="Profile-Publication">
          <Masonry
            breakpointCols={3}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column">
            {Publication.map((publi) => {
              return (
                <div
                  className="Profile-Publication-Card"
                  key={publi.id_publication}
                >
                  <div className="Profile-Publication-CardWrapper">
                    {
                      publi.video ? <video controls className='Profile-Publication-img' src={publi.file}></video> : <LazyLoadImage
                        alt={publi.file}
                        className="Profile-Publication-img"
                        effect='blur'
                        // height={300}
                        key={publi.id_publication}
                        // placeholderSrc={showLowResImages ? photo.lowResSrc : null}
                        // scrollPosition={scrollPosition}
                        src={publi.file}
                        threshold={1}
                        wrapperClassName="gallery-img-wrapper" />
                    }

                    {/* <img src={publi.file} alt="" /> */}
                  </div>
                </div>
              );
            })}
          </Masonry>

        </div>
      </div>
    </div>
  )
}
