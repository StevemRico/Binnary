import React, { useState, useEffect, useContext, Suspense, lazy } from "react";
import UserContext from "../../context/User/UserContext";
import { GetPublicationsUser } from "../../services/Publications.service";
import { useLocalStorage } from "../../hooks/useLocalStorage";
import "../Profile/Profile.css";
import { useParams, Link, useNavigate } from "react-router-dom";
import { FollowUnFollowService, GetUserProfile, PostNotification } from "../../services/User.service";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { BsArrowBarDown } from 'react-icons/bs'
import { AiFillSetting } from 'react-icons/ai'
import { CreateConversation } from '../../services/Message.services';
import Masonry from 'react-masonry-css'
export default function Profile() {
  const [UserC, setUserC] = useLocalStorage('user', '');
  const [Token, setToken] = useLocalStorage("token", " ");
  const [Receiver, setReceiver] = useLocalStorage("receiver", " ");
  const [Publication, setPublication] = useState([]);
  const [User, setUser] = useState({});
  const { chat, GetChat } = useContext(UserContext);
  let { id } = useParams();
  let UserTF = null;
  useEffect(() => {
    GetUserProfile(Token, id).then(user => setUser(user));
    GetPublicationsUser(Token, id).then((publi) =>
      setPublication(publi)
    );
    setReceiver(id);
    // console.log(id);
  }, [id]);
  if (UserC.id_user === parseInt(id)) {
    UserTF = true
  } else {
    UserTF = false
  }
  const NavigateMessage = () => {
    const Members = {
      "sender": UserC.id_user,
      "receiver": parseInt(Receiver)
    }
    CreateConversation(Token, Members).then(conver => {/*console.log(conver);*/ GetChat(conver); });
    navigate('/Messages');
  }
  const Follow = {
    "follow": parseInt(Receiver),
    "follower": UserC.id_user
  }
  const FollowUnFollow = () => {
    FollowUnFollowService(Token, Follow).then()
    PostNotification(Token, { sender: UserC.id_user, receiver: id, text: "Te sigio" });
    window.location.reload();
  }

  let navigate = useNavigate();
  return (
    <div className="Profile">
      <div className="ProfileWrapper">
        <div className="Profile-Info">
          <div className="Profile-Info-Img">
            <img src={User.profile_image} alt="" />
          </div>
          <div className="Profile-Info-Description">
            <div className="Profile-Info-Description-Info">
              <span>
                {User.username}
                {
                  UserTF ? <Link to={{ pathname: `/ProfileSettings/${UserC.id_user}` }} className='LinkProfileSettings'><AiFillSetting /></Link> : ""
                }
              </span>
              <p>{User.description}</p>
              <div className="Profile-Info-Description-Info-FFP-Wrapper">
                <div className="Profile-Info-Description-Info-FFP">
                  <div>{Publication.length} Publications</div>
                  <div>{User.followers_length} Followers</div>
                  <div>{User.follow_length} Follows</div>
                </div>
                <div className="Profile-Info-Description-Info-Message">
                  <div>{UserTF ? "" : <button className='Profile-Follow' onClick={() => FollowUnFollow()}>Seguir</button>}</div>
                  {
                    UserTF ? "" : <button className="LinkCreateConversation" onClick={() => NavigateMessage()}>Message</button>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="Profile-Publication">
          <Masonry
            breakpointCols={3}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column">

            {Publication.map((publi) => {
              return (
                <div
                  className="Profile-Publication-Card"
                  key={publi.id_publication}
                >
                  <div className="Profile-Publication-CardWrapper">
                    {
                      publi.video ? <video controls className='Profile-Publication-img' src={publi.file}></video> : <LazyLoadImage
                        alt={publi.src}
                        className="Profile-Publication-img"
                        effect='blur'
                        key={publi.id_publication}
                        src={publi.file}
                        threshold={1}
                        wrapperClassName="gallery-img-wrapper" />
                    }
                  </div>
                </div>
              );
            })}
          </Masonry>
        </div>
      </div>
    </div>
  );

}
