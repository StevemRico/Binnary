import React, { useContext, useEffect, useState } from 'react'
import {Link} from 'react-router-dom'
import './FindUser.css'
import UserContext from '../../context/User/UserContext'
import { GetFindUser } from '../../services/User.service';
import {useLocalStorage} from '../../hooks/useLocalStorage';
export default function FindUser() {
    const {search} = useContext(UserContext);
    const [Finds, setFinds] = useState([]);
    const [Token, setToken] = useLocalStorage('token','');
    useEffect(()=>{
        GetFindUser(Token,search).then(findUser => setFinds(findUser));
    },[search])
    return (
        <div className="FindUser">
            {Finds.map(users=>{
                return(
                    <Link to={{pathname: `/Profile/${users.id_user}`}} className='LinkSearch' key={users.id_user}>
                    <div className="FindUser-Card" >
                        <img src={users.profile_image} alt="" />
                        <span>{users.username}</span>
                    </div>
                    </Link>
                )
            })}
        </div>
    )
}
