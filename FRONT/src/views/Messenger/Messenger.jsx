import React, { useContext, useEffect, useRef, useState } from 'react'
import Conversations from '../../components/Conversations/Conversations';
import MessagePost from '../../components/MessagePost/MessagePost';
import Messages from '../../components/Messages/Messages';
import './Messenger.css';

export default function Messenger() {
    return (
        <div className="Messenger">
            <div className="ChatMenu">
                <div className="ChatMenuWrapper">
                    {/* <input type="text" placeholder='Busca amigos' className='ChatMenuInput' /> */}
                    <Conversations/>
                </div>
            </div>
            <div className="ChatBox">
                <div className="ChatBoxWrapper">
                    <div className="ChatBoxTop">
                        <Messages />
                    </div>
                </div>
            </div>
        </div>
    )
}
