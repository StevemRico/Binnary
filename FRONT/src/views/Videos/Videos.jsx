import React, { useEffect, useState } from 'react'
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { GetVideos } from '../../services/Publications.service';
import { Link } from 'react-router-dom'
import './Videos.css';
import { FcComments, FcShare } from 'react-icons/fc'
import Likes from '../../components//Like//Likes';
import Comments from '../../components/Comments/Comments';
import LikeCount from '../../components/Like/LikeCount';
export default function Videos() {
    const [Token, setToken] = useLocalStorage('token', '');
    const [Video, setVideo] = useState([]);
    useEffect(() => {
        GetVideos(Token).then(videos => setVideo(videos));
    }, [])
    // console.log(Publication);
    return (
        <div className="Video">
            <div className="VideoWrapper">
                {
                    Video.map((publi => {
                        return (
                            <div className="Video-card" key={publi.id_publication}>
                                <div className="Video-img">
                                    <video src={publi.file} controls>
                                    </video>
                                </div>
                                <div className="Video-card-bottom">
                                    <div className="Video-icons">
                                        <div className="Video-icons-like">
                                            <Likes like={publi.like} likeUser={publi.likeuser} publi={publi.id_publication} />
                                        </div>
                                        <div className="Video-icons-comment">
                                            <FcComments />
                                        </div>
                                        <div className="Video-icons-share">
                                            <FcShare />
                                        </div>
                                    </div>
                                    <div className="Video-likes">
                                        {/* <LikeCount lenght={publi.likes.length} publi={publi.id_publication} /> */}
                                    </div>
                                    <div className="Video-description">
                                        <span>
                                            {publi.descripcion}
                                        </span>
                                    </div>
                                    <div className="Video-comments">
                                        {/* <Comments comments={publi.comments} /> */}
                                        <span>
                                            {publi.comments.length} Comentarios...
                                        </span>
                                        <br />
                                        <input type="text" name="" id="" />
                                    </div>
                                </div>
                            </div>
                        )
                    }))
                }
            </div>
        </div>
    )
}